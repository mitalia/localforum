#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib2
import urllib
import urlparse
import re
import datetime
from cookielib import CookieJar
from bs4 import BeautifulSoup


class HTMLit:

    class Post:
        ID=None
        user=None
        userID=None
        date=None
        title=None
        content=None

        def __repr__(self):
            return repr(self.__dict__)

    class Thread:

        def __init__(self):
            self.ID=None
            self.title=None
            self.forumID=0
            self.posts=[]

        def __repr__(self):
            return repr(self.__dict__)

    def __init__(self):
        self.cookieJar = CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookieJar))
        self.baseURL = "http://forum.html.it/forum/"
        self.loggedIn=False
        self.timeout=10

        self.opener.addheaders = [('User-agent','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:18.0) Gecko/20100101 Firefox/18.0')]

    def GetPageWithEncoding(self, page, formData=None, absolute=False):

        if not absolute:
            page=self.baseURL+page
        encodedData=None
        if formData is not None:
            encodedData = urllib.urlencode(formData)
        response=self.opener.open(page, encodedData, timeout=self.timeout)
        ret=response.read()
        encoding=response.headers['content-type'].split('charset=')[-1]
        return {'data': ret, 'encoding': encoding}

    def GetPage(self, page, formData=None, absolute=False):
        return self.GetPageWithEncoding(page, formData, absolute)['data']

    def GetPageSoup(self, page, formData=None, absolute=False):
        data=self.GetPageWithEncoding(page, formData, absolute)
        return BeautifulSoup(data['data'], from_encoding=data['encoding'])

    def Login(self, username, password):
        # Get the home page
        index=self.GetPageSoup("index.php")
        # Extract the magic key
        s=index('form',attrs={'name': 'member'})[0]('input',attrs={'name':'s', 'type': 'hidden'})[0]['value']
        # Perform login
        self.GetPage('member.php', {'action': 'login', 'username': username, 'password': password, 's': s})
        # Check if login was successful
        cp=self.GetPageSoup("usercp.php")
        if len(cp(text=re.compile("Non sei collegato al forum")))>=1:
            raise RuntimeError("Invalid username/password.")
        self.loggedIn=True

    def PostThread(self, forumID, subject, body, parseURL=False, emailNotification=False, showSignature=True):
        if not self.loggedIn:
            raise RuntimeError("Not logged in.")
        data={
                's': "",
                'forumid': str(forumID),
                'action': 'postthread',
                'subject': subject,
                'submit': 'Invia la nuova discussione',
                'message': body
            }
        if parseURL:
            data['parseurl']='yes'
        if emailNotification:
            data['email']='yes'
        if showSignature:
            data['signature']='yes'
        self.GetPage("newthread.php", data)

    def GetThread(self, threadID):
        threadPage = "showthread.php?threadid=%d&perpage=40&pagenumber=%d"
        downloadPage = "showthread.php?threadid=%d&action=download"
        firstPage=self.GetPageSoup(threadPage % (threadID, 1))
        try:
            cookieJar.clear("forum.html.it", "/", "bbthreadview")
            print "bbthreadview cleared"
        except:
            pass

        posts=[]

        plainThread=unicode(self.GetPage(downloadPage % threadID),'windows-1252','replace')

        separator="-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"
        postsRe=re.compile("\r\n\\[Post \\d+\\]\r\nAutore : (.*)\r\nData : (.*)\r\n(?:Titolo : (.*)){0,1}\r\n((?:.*\r{0,1}\n)*?)\r\n"+separator)
        pc=0
        for m in postsRe.findall(plainThread, re.M):
            p=HTMLit.Post()
            p.content=m[3]
            p.title=m[2]
            p.date=datetime.datetime.strptime(m[1], u"%d-%m-%Y %H:%M")
            p.user=BeautifulSoup(m[0]).text
            p.userID=0
            posts.append(p)

        try:
            pageNumber=int(dict(urlparse.parse_qsl(urlparse.urlparse(firstPage('a', text='>', href=re.compile('showthread\\.php\\?.*threadid=%d' % threadID), limit=1)[0]['href']).query))['pagenumber'])
        except:
            pageNumber=1

        title=firstPage('a', href='#', limit=1)[0].string


        idRe=re.compile('^post(\\d+)$')
        uIDRe=re.compile('^member2\\.php\\?.*action=addlist.*userid=(\\d+)')
        uNameRe=re.compile('^Aggiungi l\'utente (.*) alla tua lista degli utenti amici$')

        for i in range(1, pageNumber+1):
            if i==1:
                page=firstPage
            else:
                page=self.GetPageSoup(threadPage % (threadID, i))

            pc=(i-1)*40

            for ap in page('a', attrs={'name': idRe}):
                p=posts[pc]
                p.ID=int(idRe.search(ap['name']).group(1))
                pc+=1

            pc=(i-1)*40

            for au in page('a', href=uIDRe):
                ai=au('img', alt=uNameRe)[0]
                uID=int(uIDRe.search(au['href']).group(1))
                user=uNameRe.search(ai['alt']).group(1)
                while posts[pc].user!=user:
                    pc+=1
                posts[pc].userID=uID
                pc+=1


        t=HTMLit.Thread()
        t.ID=threadID
        t.title=title
        t.posts=posts
        return t

    def GetArchiveThreadsID(self, forumID):
        archPage="forumdisplay/f-%d-p-%d.html"

        firstPage=self.GetPageSoup(archPage % (forumID, 1))
        pageRe=re.compile("http://forum\\.html\\.it/forum/forumdisplay/f-(\\d+)-p-(\\d+)\\.html")
        threadRe=re.compile("http://forum\\.html\\.it/forum/showthread/t-(\\d+)\\.html")

        try:
            pageNumber=max([int(pageRe.search(ap['href']).group(2)) for ap in firstPage('a', href=pageRe)])
        except:
            pageNumber=1

        threads=set()

        for i in range(1, pageNumber+1):
            page=None
            tries=0
            while True:
                try:
                    print "Page", i
                    if i==1:
                        page=firstPage
                    else:
                        page=self.GetPageSoup(archPage % (forumID, i))
                    break
                except:
                    tries+=1
                    if tries>3:
                        raise

            for at in page('a', href=threadRe):
                threads.add(int(threadRe.search(at['href']).group(1)))


        return sorted(list(threads))

if __name__ == "__main__":
    f=HTMLit()
    print f.GetThread(263571)
