#!/usr/bin/env python
# -*- coding: utf-8 -*-

import forumstorage
import htmlit
import cPickle
import os

forumID=7
onlyNotPresent=True
#forumID=31
restartFrom=None
forum=htmlit.HTMLit()
#forum.Login("username","password")
fileName="/home/matteo/Scrivania/ot.sqlite"
fileExists=os.path.isfile(fileName)
fs=forumstorage.ForumStorage(fileName)
if not fileExists:
    fs.createSchema()
toRetry=[]
print "Carico thread ID dagli archivi..."
threadIDs=None
try:
    with open("lastthreadIDs.dat", "r") as f:
        threadIDs=cPickle.load(f)
except:
    print "Impossibile aprire thread ID salvati"
if not threadIDs:
    threadIDs=forum.GetArchiveThreadsID(forumID)
with open("lastthreadIDs.dat", "w") as f:
    cPickle.dump(threadIDs, f)

if onlyNotPresent:
    threadIDs=sorted(list(set(threadIDs)-set(fs.getStoredThreadIDs(forumID))))

if restartFrom:
    #toRetry.append(restartFrom)
    threadIDs=threadIDs[threadIDs.index(restartFrom):]

transactCoda=[]

for threadID in threadIDs:
    print "Thread", threadID
    try:
        t=forum.GetThread(threadID)
        if len(t.posts)==0:
            raise RuntimeError("Zero-posts thread!")
        print t.posts[0].date
        t.forumID=forumID
        transactCoda.append(t)
    except Exception as ex:
        print "Eccezione (retrieval): ", repr(ex)
        toRetry.append(threadID)

    try:
        if(len(transactCoda)>=20 or threadID==threadIDs[-1]):
            print "Committing..."
            fs.storeThreads(transactCoda)
            transactCoda=[]
    except Exception as ex:
        print "Eccezione (storage): ", repr(ex)
        toRetry.extend([t.ID for t in transactCoda])

print "Da riprovare: ", repr(toRetry)

with open("toretry.dat", "w") as f:
    cPickle.dump(toRetry, f)
