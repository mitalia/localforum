#!/usr/bin/env python
# -*- coding: utf-8 -*-
from htmlit import *
import sqlite3

class ForumStorage:
    conn=None
    dbPath=None

    def __init__(self, dbPath):
        self.dbPath=dbPath
        self.conn=sqlite3.connect(dbPath, detect_types=sqlite3.PARSE_DECLTYPES)

    def createSchema(self):
        try:
            c=self.conn.cursor()
            c.execute("""
                -- Descrizione POSTS
                CREATE TABLE posts (
                    "ID" INTEGER PRIMARY KEY NOT NULL,
                    "title" TEXT,
                    "userID" INTEGER NOT NULL,
                    "userName" TEXT NOT NULL,
                    "content" TEXT NOT NULL,
                    "date" TIMESTAMP NOT NULL,
                    "threadID" INTEGER NOT NULL
                )
                """)
            c.execute("""
                -- Descrizione THREADS
                CREATE TABLE "threads" (
                    "ID" INTEGER PRIMARY KEY NOT NULL,
                    "title" TEXT NOT NULL,
                    "startdate" TIMESTAMP NOT NULL,
                    "forumID" INTEGER NOT NULL
                )
                """)
            c.close()
            self.conn.commit()
        except:
            self.conn.rollback()
            raise

    def storeThreads(self, threads):
        try:
            c=self.conn.cursor()
            for thread in threads:
                c.execute("insert or replace into threads values (?, ?, ?, ?)", (thread.ID, thread.title, thread.posts[0].date, thread.forumID))
                for p in thread.posts:
                    c.execute("insert or replace into posts values (?, ?, ?, ?, ?, ?, ?)", (p.ID, p.title, p.userID, p.user, p.content, p.date, thread.ID))
            c.close()
            self.conn.commit()
        except:
            self.conn.rollback()
            raise

    def storeThread(self, thread):
        self.storeThreads([thread])

    def getStoredThreadIDs(self, forumID):
        c=self.conn.cursor()
        c.execute("select id from threads where forumID = (?)", (forumID,))
        out=[row[0] for row in c]
        c.close()
        return out

if __name__ == "__main__":
    f=HTMLit()
    fs=ForumStorage("./localforum.sqlite")
    fs.storeThread(f.GetThread(69002))
