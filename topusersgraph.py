#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sqlite3
import datetime
import dateutil.relativedelta
from dateutil.rrule import *
import dateutil.parser

dbPath="./localforum.sqlite"
outPath="topusers.tsv"
intervalsRes=2
minPosts=intervalsRes*30*2

conn=sqlite3.connect(dbPath)
c=conn.cursor()
c.execute("select min(date), max(date) from posts")
startDate, endDate=tuple(dateutil.parser.parse(d) for d in c.fetchone())
startDate, endDate=startDate.date(), endDate.date()
c.close()
steps=list(rrule(MONTHLY, interval=intervalsRes, dtstart=startDate, until=endDate))
diff=dateutil.relativedelta.relativedelta(endDate,startDate)
print "Period: ", startDate, " - ", endDate, "(", diff,")"
users=dict()
users["totale"]=[0]*(len(steps)-1)
for i in range(0, len(steps)-1):
    minDate=steps[i]
    maxDate=steps[i+1]
    print minDate, maxDate
    c=conn.cursor()
    c.execute("""
                select count(username) as postsnumber, username
                from posts
                where
                        userid<>0
                    and
                        date>=(?)
                    and
                        date<(?)
                group by userid
                """, (minDate, maxDate))
    tot=0
    for r in c:
        tot+=r[0]
        if r[0]>=minPosts:
            userBins=users.get(r[1], [None]*(len(steps)-1))
            userBins[i]=r[0]
            users[r[1]]=userBins
    users["totale"][i]=tot
    c.close()

conn.close()

with open(outPath, "w") as f:
    f.write("Date\t")
    for d in steps:
        f.write(str(d.date())+"\t")
    f.write("\n")

    for user, bins in users.iteritems():
        f.write(unicode(user).encode('utf-8')+"\t")
        for c in bins:
            if c is not None:
                f.write("%d" % c)
            f.write("\t")
        f.write("\n")
